module com.alex.docdb {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.java;
    requires junit;

    opens com.alex.docdb to javafx.fxml;
    opens com.alex.docdb.controller to javafx.fxml;
    opens com.alex.docdb.controller.tab to javafx.fxml;
    opens com.alex.docdb.controller.dialog to javafx.fxml;
    opens test to junit;

    exports com.alex.docdb;
    exports com.alex.docdb.controller;
    exports com.alex.docdb.controller.tab;
    exports com.alex.docdb.model.user;
    exports com.alex.docdb.model.doc;

}
