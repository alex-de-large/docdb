/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alex.docdb.controller;

import com.alex.docdb.App;
import com.alex.docdb.db.exceptions.EntityNotFoundException;
import com.alex.docdb.db.impl.UserDAO;
import com.alex.docdb.model.user.User;
import com.alex.docdb.model.user.UserSession;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;

/**
 *
 * @author alex_delarge
 */
public class LoginController {
    
    @FXML
    private Button loginButton;
    @FXML
    private TextField usernameTextField;
    @FXML
    private PasswordField passwordField;

    private UserDAO userDAO;

    public LoginController() {
        userDAO = new UserDAO();
    }
    
    @FXML
    private void login(ActionEvent event) throws IOException {
        User user = null;
        try {
            user = userDAO.findEntityById(usernameTextField.getText());
        } catch (EntityNotFoundException ex) {
            showMessage("No such user.");
            return;
        }
        if (checkCredits(user)) {
            UserSession.get(user);
            App.setRoot("home");
        } else {
            showMessage("Incorrect password.");
        }
    }
    
    private boolean checkCredits(User user) {
        return user.getPassword().equals(passwordField.getText());
    }

    private void showMessage(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Login Error");
        alert.setHeaderText("Error");
        alert.setContentText(text);

        alert.showAndWait();
    }
    
}
