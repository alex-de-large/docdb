package com.alex.docdb.controller.dialog;

import com.alex.docdb.App;
import com.alex.docdb.model.doc.Documentation;
import com.alex.docdb.model.doc.Project;
import com.alex.docdb.model.user.Group;
import com.alex.docdb.model.user.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Window;

import java.io.IOException;

public class AddProjectDialog extends Dialog<Project> {

    @FXML
    private TextField idTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField documentationIdTextField;
    @FXML
    private Button addButton;


    public AddProjectDialog(Window owner) {
        this(owner, null);
    }

    public AddProjectDialog(Window owner, Project project) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("fxml/dialog_project.fxml"));
        loader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = loader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);

        setResizable(true);
        setDialogPane(dialogPane);

        if (project != null) {
            idTextField.setText(Integer.toString(project.getId()));
            idTextField.setEditable(false);
            nameTextField.setText(project.getName());
            documentationIdTextField.setText(Integer.toString(project.getDocumentation().getId()));
        }
    }

    @FXML
    private void create(ActionEvent event) {
        int id = 0;
        String name = null;
        int documentationId = 0;
        try {
            id = Integer.parseInt(idTextField.getText());
            name = nameTextField.getText();
            documentationId = Integer.parseInt(documentationIdTextField.getText());
        } catch (Exception ex) {
            close();
        }
        setResult(new Project(id, name, new Documentation(documentationId, null), null));
        close();
    }
}
