package com.alex.docdb.controller.dialog;

import com.alex.docdb.App;
import com.alex.docdb.model.user.Group;
import com.alex.docdb.model.user.GroupPermission;
import com.alex.docdb.model.user.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Window;

import java.io.IOException;

public class AddUserDialog extends Dialog<User> {

    @FXML
    private TextField idTextField;
    @FXML
    private TextField usernameTextField;
    @FXML
    private TextField passwordTextField;
    @FXML
    private TextField groupIdTextField;
    @FXML
    private Button addButton;


    public AddUserDialog(Window owner) {
        this(owner, null);
    }

    public AddUserDialog(Window owner, User user) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("fxml/dialog_user.fxml"));
        loader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = loader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);

        setResizable(true);
        setDialogPane(dialogPane);

        if (user != null) {
            idTextField.setText(Integer.toString(user.getId()));
            idTextField.setEditable(false);
            usernameTextField.setText(user.getUsername());
            passwordTextField.setText(user.getPassword());
            groupIdTextField.setText(Integer.toString(user.getGroup().getId()));
        }
    }

    @FXML
    private void create(ActionEvent event) {
        int id = 0;
        String username = null;
        String password = null;
        int groupId = 0;
        try {
            id = Integer.parseInt(idTextField.getText());
            username = usernameTextField.getText();
            password = passwordTextField.getText();
            groupId = Integer.parseInt(groupIdTextField.getText());
        } catch (Exception ex) {
            close();
        }
        setResult(new User(id, username, password, new Group(groupId, null, null)));
        close();
    }
}
