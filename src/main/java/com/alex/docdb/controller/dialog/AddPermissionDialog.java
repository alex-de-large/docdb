package com.alex.docdb.controller.dialog;

import com.alex.docdb.App;
import com.alex.docdb.model.user.Permission;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Window;

import java.io.IOException;
import java.util.Optional;

public class AddPermissionDialog extends Dialog<Permission> {

    @FXML
    private TextField idTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private Button addButton;

    public AddPermissionDialog(Window owner) {
        this(owner, null);
    }
    public AddPermissionDialog(Window owner, Permission permission) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("fxml/dialog_permission.fxml"));
        loader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = loader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);

        setResizable(true);
        setDialogPane(dialogPane);

        if (permission != null) {
            idTextField.setText(Integer.toString(permission.getId()));
            nameTextField.setText(permission.getName());
        }
    }


    @FXML
    private void createPermission(ActionEvent event) {
        int id = 0;
        String name = null;
        try {
            id = Integer.parseInt(idTextField.getText());
            name = nameTextField.getText();
        } catch (Exception ex) {
            return;
        }
        setResult(new Permission(id, name));
        close();
    }
}
