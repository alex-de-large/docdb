package com.alex.docdb.controller.dialog;

import com.alex.docdb.App;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Window;
import javafx.util.Pair;

import java.io.IOException;

public class AddProjectEmployeeDialog extends Dialog<Pair<Integer, Integer>> {
    @FXML
    private TextField projectTextField;
    @FXML
    private TextField employeeTextField;

    public AddProjectEmployeeDialog(Window owner) {
        this(owner, null);
    }

    public AddProjectEmployeeDialog(Window owner, Pair<Integer, Integer> record) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("fxml/dialog_project_employee.fxml"));
        loader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = loader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);

        setResizable(true);
        setDialogPane(dialogPane);

        if (record != null) {
            projectTextField.setText(Integer.toString(record.getKey()));
            employeeTextField.setText(Integer.toString(record.getValue()));
        }
    }

    @FXML
    private void create(ActionEvent event) {
        int projectId = 0;
        int employeeId = 0;
        try {
            projectId = Integer.parseInt(projectTextField.getText());
            employeeId = Integer.parseInt(employeeTextField.getText());
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        setResult(new Pair<>(projectId, employeeId));
        close();
    }
}
