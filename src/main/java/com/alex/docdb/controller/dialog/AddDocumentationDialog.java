package com.alex.docdb.controller.dialog;

import com.alex.docdb.App;
import com.alex.docdb.model.doc.Documentation;
import com.alex.docdb.model.user.Group;
import com.alex.docdb.model.user.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Window;

import java.io.IOException;

public class AddDocumentationDialog extends Dialog<Documentation> {

    @FXML
    private TextField idTextField;
    @FXML
    private TextField folderPathTextField;
    @FXML
    private Button addButton;


    public AddDocumentationDialog(Window owner) {
       this(owner, null);
    }

    public AddDocumentationDialog(Window owner, Documentation documentation) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("fxml/dialog_documentation.fxml"));
        loader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = loader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);

        setResizable(true);
        setDialogPane(dialogPane);

        if (documentation != null) {
            idTextField.setText(Integer.toString(documentation.getId()));
            idTextField.setEditable(false);
            folderPathTextField.setText(documentation.getFolderPath());
        }
    }

    @FXML
    private void create(ActionEvent event) {
        try {
            int id = Integer.parseInt(idTextField.getText());
            String folderPath = folderPathTextField.getText();
            setResult(new Documentation(id, folderPath));
        } catch (Exception ex) {
            setResult(null);
        }
        close();
    }
}
