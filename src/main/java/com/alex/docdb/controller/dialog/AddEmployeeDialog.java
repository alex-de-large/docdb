package com.alex.docdb.controller.dialog;

import com.alex.docdb.App;
import com.alex.docdb.model.doc.Documentation;
import com.alex.docdb.model.doc.Employee;
import com.alex.docdb.model.doc.Project;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Window;

import java.io.IOException;

public class AddEmployeeDialog extends Dialog<Employee> {

    @FXML
    private TextField idTextField;
    @FXML
    private TextField firstnameTextField;
    @FXML
    private TextField lastnameTextField;
    @FXML
    private TextField ageTextField;
    @FXML
    private TextField positionTextField;
    @FXML
    private TextField experienceTextField;
    @FXML
    private Button addButton;


    public AddEmployeeDialog(Window owner) {
       this(owner, null);
    }

    public AddEmployeeDialog(Window owner, Employee employee) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("fxml/dialog_employee.fxml"));
        loader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = loader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);

        setResizable(true);
        setDialogPane(dialogPane);

        if (employee != null) {
            idTextField.setText(Integer.toString(employee.getId()));
            firstnameTextField.setText(employee.getFirstname());
            lastnameTextField.setText(employee.getLastname());
            ageTextField.setText(Integer.toString(employee.getAge()));
            positionTextField.setText(employee.getPosition());
            experienceTextField.setText(Integer.toString(employee.getExperience()));
        }
    }

    @FXML
    private void create(ActionEvent event) {
        int id = 0;
        String firstname = null;
        String lastname = null;
        int age = 0;
        String position = null;
        int experience = 0;
        try {
            id = Integer.parseInt(idTextField.getText());
            firstname = firstnameTextField.getText();
            lastname = lastnameTextField.getText();
            age = Integer.parseInt(ageTextField.getText());
            position = positionTextField.getText();
            experience = Integer.parseInt(experienceTextField.getText());
        } catch (Exception ex) {
            close();
        }
        setResult(new Employee(id, firstname, lastname, age, position, experience));
        close();
    }
}
