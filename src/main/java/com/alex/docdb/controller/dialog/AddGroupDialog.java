package com.alex.docdb.controller.dialog;

import com.alex.docdb.App;
import com.alex.docdb.model.user.Group;
import com.alex.docdb.model.user.GroupPermission;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Window;

import java.io.IOException;

public class AddGroupDialog extends Dialog<Group> {

    @FXML
    private TextField idTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField groupPermissionIdTextField;
    @FXML
    private Button addButton;


    public AddGroupDialog(Window owner) {
       this(owner, null);
    }

    public AddGroupDialog(Window owner, Group group) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("fxml/dialog_group.fxml"));
        loader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = loader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);

        setResizable(true);
        setDialogPane(dialogPane);

        if (group != null) {
            idTextField.setText(Integer.toString(group.getId()));
            idTextField.setEditable(false);
            nameTextField.setText(group.getName());
            groupPermissionIdTextField.setText(Integer.toString(group.getGroupPermission().getId()));
        }
    }

    @FXML
    private void create(ActionEvent event) {
        int id = 0;
        String name = null;
        int groupPermissionId = 0;
        try {
            id = Integer.parseInt(idTextField.getText());
            name = nameTextField.getText();
            groupPermissionId = Integer.parseInt(groupPermissionIdTextField.getText());
        } catch (Exception ex) {
            close();
        }
        setResult(new Group(id, name, new GroupPermission(groupPermissionId, 0)));
        close();
    }
}
