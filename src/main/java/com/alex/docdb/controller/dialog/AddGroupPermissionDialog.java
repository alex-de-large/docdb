package com.alex.docdb.controller.dialog;

import com.alex.docdb.App;
import com.alex.docdb.model.user.GroupPermission;
import com.alex.docdb.model.user.Permission;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Window;

import java.io.IOException;

public class AddGroupPermissionDialog extends Dialog<GroupPermission> {

    @FXML
    private TextField idTextField;
    @FXML
    private TextField codeTextField;
    @FXML
    private Button addButton;

    public AddGroupPermissionDialog(Window owner) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("fxml/dialog_group_permission.fxml"));
        loader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = loader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);

        setResizable(true);
        setDialogPane(dialogPane);
    }

    public AddGroupPermissionDialog(Window owner, GroupPermission groupPermission) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("fxml/dialog_group_permission.fxml"));
        loader.setController(this);

        DialogPane dialogPane = null;
        try {
            dialogPane = loader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        initOwner(owner);
        initModality(Modality.APPLICATION_MODAL);

        setResizable(true);
        setDialogPane(dialogPane);

        if (groupPermission != null) {
            idTextField.setText(Integer.toString(groupPermission.getId()));
            idTextField.setEditable(false);
            codeTextField.setText(Integer.toString(groupPermission.getCode()));
        }
    }

    @FXML
    private void createGroupPermission(ActionEvent event) {
        int id = 0;
        int code = 0;
        try {
            id = Integer.parseInt(idTextField.getText());
            code = Integer.parseInt(codeTextField.getText());
        } catch (Exception ex) {
            close();
        }
        setResult(new GroupPermission(id, code));
        close();
    }
}
