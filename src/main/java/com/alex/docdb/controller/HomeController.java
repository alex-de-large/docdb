package com.alex.docdb.controller;

import com.alex.docdb.App;
import com.alex.docdb.controller.tab.*;
import com.alex.docdb.model.user.User;
import com.alex.docdb.model.user.UserSession;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HomeController {

    @FXML
    private TabPane tabPane;
    @FXML
    private Button deleteButton;
    @FXML
    private Button insertButton;
    @FXML
    private Button updateButton;
    private FXMLLoader fxmlLoader;

    private User user;
    private List<DataTab> dataTabs;
    private DataTab currentDataTab;

    public HomeController() {
        user = UserSession.get(null).getUser();
        initTabs();
    }

    @FXML
    public void initialize() {
        dataTabs.forEach((dataTab) -> {
            if (user.getGroup().hasPermission(dataTab.getPermissionCodeView())) {
                tabPane.getTabs().add(new Tab(dataTab.getName()));
            }
        });

        tabPane.getSelectionModel().clearSelection();
        tabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            currentDataTab = dataTabs.stream().filter((d) -> d.getName()
                    .equals(newValue.getText())).findAny().get();

            if (newValue.getContent() == null) {
                try {
                    fxmlLoader = new FXMLLoader();
                    fxmlLoader.setController(currentDataTab.getDataTabController());
                    Parent root = fxmlLoader.load(App.class.getResource("fxml/data_tab.fxml").openStream());
                    newValue.setContent(root);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                Parent root = (Parent) newValue.getContent();
            }
        });
        tabPane.getSelectionModel().selectFirst();

        deleteButton.setOnAction(actionEvent -> {
            if (!user.getGroup().hasPermission(currentDataTab.getPermissionCodeDelete())) {
                return;
            }
            currentDataTab.getDataTabController().onDelete();
        });

        insertButton.setOnAction(actionEvent -> {
            if (!user.getGroup().hasPermission(currentDataTab.getPermissionCodeAdd())) {
                return;
            }
            currentDataTab.getDataTabController().onInsert();
        });

        updateButton.setOnAction(actionEvent -> {
            if (!user.getGroup().hasPermission(currentDataTab.getPermissionCodeUpdate())) {
                return;
            }
            currentDataTab.getDataTabController().onUpdate();
        });
    }

    private void initTabs() {
        dataTabs = new ArrayList<>();
        dataTabs.add(new DataTab(16, 32, 64, 128, "User", new UserDataController()));
        dataTabs.add(new DataTab(16, 32, 64, 128,"Permission", new PermissionDataController()));
        dataTabs.add(new DataTab(16, 32, 64, 128,"Group", new GroupDataController()));
        dataTabs.add(new DataTab(16, 32, 64, 128,"GroupPermission", new GroupPermissionDataController()));
        dataTabs.add(new DataTab(1, 2, 4, 8,  "Employee", new EmployeeDataController()));
        dataTabs.add(new DataTab(1, 2, 4, 8, "Documentation", new DocumentationDataController()));
        dataTabs.add(new DataTab(1, 2, 4, 8, "Project", new ProjectTabController()));
        dataTabs.add(new DataTab(1, 2, 4, 8, "ProjectEmployee", new ProjectEmployeeTabController()));
    }
}
