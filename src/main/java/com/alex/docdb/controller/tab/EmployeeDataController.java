package com.alex.docdb.controller.tab;

import com.alex.docdb.App;
import com.alex.docdb.controller.dialog.AddEmployeeDialog;
import com.alex.docdb.db.impl.EmployeeDAO;
import com.alex.docdb.model.doc.Employee;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;
import java.util.Optional;

public class EmployeeDataController extends DataTabController<Employee> {

    private EmployeeDAO employeeDAO;

    public EmployeeDataController() {
        employeeDAO = new EmployeeDAO();
    }

    @Override
    protected void setUpUI(TableView<Employee> tableView, List<Employee> data) {
        TableColumn<Employee, Integer> idColumn = new TableColumn<>("id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableView.getColumns().add(idColumn);

        TableColumn<Employee, String> firstNameColumn = new TableColumn<>("firstname");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        tableView.getColumns().add(firstNameColumn);

        TableColumn<Employee, String> lastnameColumn = new TableColumn<>("lastname");
        lastnameColumn.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        tableView.getColumns().add(lastnameColumn);

        TableColumn<Employee, Integer> ageColumn = new TableColumn<>("age");
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
        tableView.getColumns().add(ageColumn);

        TableColumn<Employee, String> positionColumn = new TableColumn<>("position");
        positionColumn.setCellValueFactory(new PropertyValueFactory<>("position"));
        tableView.getColumns().add(positionColumn);

        TableColumn<Employee, Integer> experienceColumn = new TableColumn<>("experience");
        experienceColumn.setCellValueFactory(new PropertyValueFactory<>("experience"));
        tableView.getColumns().add(experienceColumn);
    }

    @Override
    protected List<Employee> getData() {
        return employeeDAO.findAll();
    }

    @Override
    public void onDelete() {
        Employee employee = getTableView().getSelectionModel().getSelectedItem();
        employeeDAO.deleteById(employee.getId());
        getTableView().getItems().remove(employee);
    }

    @Override
    public void onInsert() {
        AddEmployeeDialog addEmployeeDialog = new AddEmployeeDialog(App.getStage());
        Optional<Employee> employeeOptional = addEmployeeDialog.showAndWait();
        if (employeeOptional.isPresent()) {
            if (employeeDAO.create(employeeOptional.get())) {
                getTableView().getItems().add(employeeOptional.get());
            }
        }
    }

    @Override
    public void onUpdate() {
        AddEmployeeDialog addEmployeeDialog = new AddEmployeeDialog(App.getStage(),
                getTableView().getSelectionModel().getSelectedItem());
        Optional<Employee> employeeOptional = addEmployeeDialog.showAndWait();
        if (employeeOptional.isPresent()) {
            employeeDAO.update(employeeOptional.get());
            getTableView().getItems().clear();
            getTableView().getItems().addAll(getData());
        }
    }
}
