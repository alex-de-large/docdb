package com.alex.docdb.controller.tab;

import com.alex.docdb.App;
import com.alex.docdb.controller.dialog.AddProjectEmployeeDialog;
import com.alex.docdb.db.impl.ProjectDAO;
import com.alex.docdb.db.impl.ProjectEmployeeDAO;
import com.alex.docdb.model.doc.Employee;
import com.alex.docdb.model.doc.Project;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProjectEmployeeTabController extends DataTabController<Pair<Integer, Integer>> {

    private ProjectEmployeeDAO projectEmployeeDAO;

    public ProjectEmployeeTabController() {
        projectEmployeeDAO = new ProjectEmployeeDAO();
    }

    @Override
    protected void setUpUI(TableView<Pair<Integer, Integer>> tableView, List<Pair<Integer, Integer>> data) {
        TableColumn<Pair<Integer, Integer>, Number> projectColumn = new TableColumn<>("project");
        projectColumn.setCellValueFactory(d -> new SimpleIntegerProperty(d.getValue().getKey()));
        tableView.getColumns().add(projectColumn);

        TableColumn<Pair<Integer, Integer>, Number> employeeColumn = new TableColumn<>("employee");
        employeeColumn.setCellValueFactory(d -> new SimpleIntegerProperty(d.getValue().getValue()));
        tableView.getColumns().add(employeeColumn);
    }

    @Override
    protected List<Pair<Integer, Integer>> getData() {
//        ProjectDAO projectDAO = new ProjectDAO();
//        List<Pair<Integer, Integer>> pairs = new ArrayList<>();
//        for (Project p: projectDAO.findAll()) {
//            for (Employee e: p.getEmployees()) {
//                pairs.add(new Pair<>(p.getId(), e.getId()));
//            }
//        }
//        return pairs;
        return projectEmployeeDAO.findAll();
    }

    @Override
    public void onDelete() {
        Pair<Integer, Integer> selected = getTableView().getSelectionModel().getSelectedItem();
        projectEmployeeDAO.deleteById(selected.getKey());
        getTableView().getItems().remove(selected);
    }

    @Override
    public void onInsert() {
        AddProjectEmployeeDialog addProjectEmployeeDialog = new AddProjectEmployeeDialog(App.getStage());
        Optional<Pair<Integer, Integer>> record = addProjectEmployeeDialog.showAndWait();
        if (record.isPresent()) {
            if (projectEmployeeDAO.create(record.get())) {
                getTableView().getItems().add(record.get());
            }
        }
    }

    @Override
    public void onUpdate() {
        AddProjectEmployeeDialog addProjectEmployeeDialog = new AddProjectEmployeeDialog(App.getStage(),
                getTableView().getSelectionModel().getSelectedItem());
        Optional<Pair<Integer, Integer>> record = addProjectEmployeeDialog.showAndWait();
        if (record.isPresent()) {
            projectEmployeeDAO.update(record.get());
            getTableView().getItems().clear();
            getTableView().getItems().addAll(getData());
        }
    }
}
