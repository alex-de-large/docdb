package com.alex.docdb.controller.tab;

import com.alex.docdb.App;
import com.alex.docdb.controller.dialog.AddPermissionDialog;
import com.alex.docdb.db.impl.PermissionDAO;
import com.alex.docdb.model.user.Permission;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;
import java.util.Optional;

public class PermissionDataController extends DataTabController<Permission> {

    private PermissionDAO permissionDAO;

    public PermissionDataController() {
        permissionDAO = new PermissionDAO();
    }

    @Override
    protected void setUpUI(TableView<Permission> tableView, List<Permission> data) {
        TableColumn<Permission, Integer> idColumn = new TableColumn<>("id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableView.getColumns().add(idColumn);

        TableColumn<Permission, String> nameColumn = new TableColumn<>("name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableView.getColumns().add(nameColumn);
    }

    @Override
    protected List<Permission> getData() {
        return permissionDAO.findAll();
    }

    @Override
    public void onDelete() {
        Permission p = getTableView().getSelectionModel().getSelectedItem();
        permissionDAO.deleteById(p.getId());
        getTableView().getItems().remove(p);
    }

    @Override
    public void onInsert() {
        AddPermissionDialog addPermissionDialog = new AddPermissionDialog(App.getStage());
        Optional<Permission> permissionOptional = addPermissionDialog.showAndWait();
        if (permissionOptional.isPresent()) {
            if (permissionDAO.create(permissionOptional.get())) {
                getTableView().getItems().add(permissionOptional.get());
            }
        }
    }

    @Override
    public void onUpdate() {
        AddPermissionDialog addPermissionDialog = new AddPermissionDialog(App.getStage(),
                getTableView().getSelectionModel().getSelectedItem());
        Optional<Permission> permissionOptional = addPermissionDialog.showAndWait();
        if (permissionOptional.isPresent()) {
            permissionDAO.update(permissionOptional.get());
            getTableView().getItems().clear();
            getTableView().getItems().addAll(getData());
        }
    }
}
