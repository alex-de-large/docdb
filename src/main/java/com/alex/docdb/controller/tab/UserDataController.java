package com.alex.docdb.controller.tab;

import com.alex.docdb.App;
import com.alex.docdb.controller.dialog.AddUserDialog;
import com.alex.docdb.db.impl.UserDAO;
import com.alex.docdb.model.user.User;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;
import java.util.Optional;

public class UserDataController extends DataTabController<User> {

    private UserDAO userDAO;

    public UserDataController() {
        userDAO = new UserDAO();
    }

    @Override
    protected void setUpUI(TableView<User> tableView, List<User> data) {
        TableColumn<User, Integer> idColumn = new TableColumn<>("id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableView.getColumns().add(idColumn);
        tableView.getSortOrder().add(idColumn);

        TableColumn<User, String> usernameColumn = new TableColumn<>("username");
        usernameColumn.setCellValueFactory(new PropertyValueFactory<>("username"));
        tableView.getColumns().add(usernameColumn);

        TableColumn<User, String> passwordColumn = new TableColumn<>("password");
        passwordColumn.setCellValueFactory(new PropertyValueFactory<>("password"));
        tableView.getColumns().add(passwordColumn);

        TableColumn<User, Number> groupIdColumn = new TableColumn<>("groupId");
        groupIdColumn.setCellValueFactory(d -> new SimpleIntegerProperty(d.getValue().getGroup().getId()));
        tableView.getColumns().add(groupIdColumn);
    }

    @Override
    protected List<User> getData() {
        return userDAO.findAll();
    }

    @Override
    public void onDelete() {
        User user = getTableView().getSelectionModel().getSelectedItem();
        userDAO.deleteById(Integer.toString(user.getId()));
        getTableView().getItems().remove(user);
    }

    @Override
    public void onInsert() {
        AddUserDialog addUserDialog = new AddUserDialog(App.getStage());
        Optional<User> userOptional = addUserDialog.showAndWait();
        if (userOptional.isPresent()) {
            if (userDAO.create(userOptional.get())) {
                getTableView().getItems().add(userOptional.get());
            }
        }
    }

    @Override
    public void onUpdate() {
        AddUserDialog addUserDialog = new AddUserDialog(App.getStage(),
                getTableView().getSelectionModel().getSelectedItem());
        Optional<User> userOptional = addUserDialog.showAndWait();
        if (userOptional.isPresent()) {
            userDAO.update(userOptional.get());
            getTableView().getItems().clear();
            getTableView().getItems().addAll(getData());
        }
    }
}
