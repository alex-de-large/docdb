package com.alex.docdb.controller.tab;

import com.alex.docdb.App;
import com.alex.docdb.controller.dialog.AddGroupDialog;
import com.alex.docdb.db.impl.GroupDAO;
import com.alex.docdb.model.user.Group;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;
import java.util.Optional;

public class GroupDataController extends DataTabController<Group> {

    private GroupDAO groupDAO;

    public GroupDataController() {
        groupDAO = new GroupDAO();
    }

    @Override
    protected void setUpUI(TableView<Group> tableView, List<Group> data) {
        TableColumn<Group, Integer> idColumn = new TableColumn<>("id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableView.getColumns().add(idColumn);

        TableColumn<Group, String> nameColumn = new TableColumn<>("name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableView.getColumns().add(nameColumn);

        TableColumn<Group, Number> groupPermissionIdColumn = new TableColumn<>("groupPermissionId");
        groupPermissionIdColumn.setCellValueFactory(d ->
                new SimpleIntegerProperty(d.getValue().getGroupPermission().getId()));
        tableView.getColumns().add(groupPermissionIdColumn);

    }

    @Override
    protected List<Group> getData() {
        return groupDAO.findAll();
    }

    @Override
    public void onDelete() {
        Group group = getTableView().getSelectionModel().getSelectedItem();
        groupDAO.deleteById(group.getId());
        getTableView().getItems().remove(group);
    }

    @Override
    public void onInsert() {
        AddGroupDialog addGroupDialog = new AddGroupDialog(App.getStage());
        Optional<Group> groupOptional = addGroupDialog.showAndWait();
        if (groupOptional.isPresent()) {
            if (groupDAO.create(groupOptional.get())) {
                getTableView().getItems().add(groupOptional.get());
            }
        }
    }

    @Override
    public void onUpdate() {
        AddGroupDialog addGroupDialog = new AddGroupDialog(App.getStage(),
                getTableView().getSelectionModel().getSelectedItem());
        Optional<Group> groupOptional = addGroupDialog.showAndWait();
        if (groupOptional.isPresent()) {
            groupDAO.update(groupOptional.get());
            getTableView().getItems().clear();
            getTableView().getItems().addAll(getData());
        }
    }
}
