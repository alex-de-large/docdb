package com.alex.docdb.controller.tab;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;

import java.util.List;

public abstract class DataTabController<T> {

    @FXML
    private TableView<T> tableView;

    public DataTabController() {

    }

    protected abstract void setUpUI(TableView<T> tableView, List<T> data);

    protected abstract List<T> getData();

    @FXML
    public void initialize() {
        List<T> data = getData();
        setUpUI(tableView, data);
        tableView.setItems(FXCollections.observableList(data));
    }

    public abstract void onDelete();

    public abstract void onInsert();

    public abstract void onUpdate();

    public TableView<T> getTableView() {
        return tableView;
    }
}
