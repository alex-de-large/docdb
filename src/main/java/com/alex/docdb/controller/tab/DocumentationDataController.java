package com.alex.docdb.controller.tab;

import com.alex.docdb.App;
import com.alex.docdb.controller.dialog.AddDocumentationDialog;
import com.alex.docdb.db.impl.DocumentationDAO;
import com.alex.docdb.model.doc.Documentation;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;
import java.util.Optional;

public class DocumentationDataController extends DataTabController<Documentation> {

    private DocumentationDAO documentationDAO;

    public DocumentationDataController() {
        documentationDAO = new DocumentationDAO();
    }

    @Override
    protected void setUpUI(TableView<Documentation> tableView, List<Documentation> data) {
        TableColumn<Documentation, Integer> idColumn = new TableColumn<>("id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableView.getColumns().add(idColumn);

        TableColumn<Documentation, String> folderPathColumn = new TableColumn<>("folderPath");
        folderPathColumn.setCellValueFactory(new PropertyValueFactory<>("folderPath"));
        tableView.getColumns().add(folderPathColumn);
    }

    @Override
    protected List<Documentation> getData() {
        return documentationDAO.findAll();
    }

    @Override
    public void onDelete() {
        Documentation documentation = getTableView().getSelectionModel().getSelectedItem();
        documentationDAO.deleteById(documentation.getId());
        getTableView().getItems().remove(documentation);
    }

    @Override
    public void onInsert() {
        AddDocumentationDialog addDocumentationDialog = new AddDocumentationDialog(App.getStage());
        Optional<Documentation> documentationOptional = addDocumentationDialog.showAndWait();
        if (documentationOptional.isPresent()) {
            if (documentationDAO.create(documentationOptional.get())) {
                getTableView().getItems().add(documentationOptional.get());
            }
        }
    }

    @Override
    public void onUpdate() {
        AddDocumentationDialog addDocumentationDialog = new AddDocumentationDialog(App.getStage(),
                getTableView().getSelectionModel().getSelectedItem());
        Optional<Documentation> documentationOptional = addDocumentationDialog.showAndWait();
        if (documentationOptional.isPresent()) {
            documentationDAO.update(documentationOptional.get());
            getTableView().getItems().clear();
            getTableView().getItems().addAll(getData());
        }
    }
}
