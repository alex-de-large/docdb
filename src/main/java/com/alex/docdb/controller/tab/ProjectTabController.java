package com.alex.docdb.controller.tab;

import com.alex.docdb.App;
import com.alex.docdb.controller.dialog.AddProjectDialog;
import com.alex.docdb.db.impl.ProjectDAO;
import com.alex.docdb.model.doc.Project;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;
import java.util.Optional;

public class ProjectTabController extends DataTabController<Project> {

    private ProjectDAO projectDAO;

    public ProjectTabController() {
        projectDAO = new ProjectDAO();
    }

    @Override
    protected void setUpUI(TableView<Project> tableView, List<Project> data) {
        TableColumn<Project, Integer> idColumn = new TableColumn<>("id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableView.getColumns().add(idColumn);

        TableColumn<Project, String> nameColumn = new TableColumn<>("name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableView.getColumns().add(nameColumn);

        TableColumn<Project, Number> docIdColumn = new TableColumn<>("docId");
        docIdColumn.setCellValueFactory(d -> new SimpleIntegerProperty(d.getValue().getDocumentation().getId()));
        tableView.getColumns().add(docIdColumn);
    }

    @Override
    protected List<Project> getData() {
        return projectDAO.findAll();
    }

    @Override
    public void onDelete() {
        Project project = getTableView().getSelectionModel().getSelectedItem();
        if (projectDAO.deleteById(project.getId())) {
            getTableView().getItems().remove(project);
        }
    }

    @Override
    public void onInsert() {
        AddProjectDialog addProjectDialog = new AddProjectDialog(App.getStage());
        Optional<Project> projectOptional = addProjectDialog.showAndWait();
        if (projectOptional.isPresent()) {
            if (projectDAO.create(projectOptional.get())) {
                getTableView().getItems().add(projectOptional.get());
            }
        }
    }

    @Override
    public void onUpdate() {
        AddProjectDialog addProjectDialog = new AddProjectDialog(App.getStage(),
                getTableView().getSelectionModel().getSelectedItem());
        Optional<Project> projectOptional = addProjectDialog.showAndWait();
        if (projectOptional.isPresent()) {
            projectDAO.update(projectOptional.get());
            getTableView().getItems().clear();
            getTableView().getItems().addAll(getData());
        }
    }
}
