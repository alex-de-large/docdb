package com.alex.docdb.controller.tab;

public class DataTab {

    private int permissionCodeView;
    private int permissionCodeAdd;
    private int permissionCodeUpdate;
    private int permissionCodeDelete;
    private String name;
    private DataTabController<?> dataTabController;

    public DataTab(int permissionCodeView,
                   int permissionCodeUpdate,
                   int permissionCodeAdd,
                   int permissionCodeDelete,
                   String name,
                   DataTabController<?> dataTabController) {
        this.permissionCodeView = permissionCodeView;
        this.permissionCodeAdd = permissionCodeAdd;
        this.permissionCodeUpdate = permissionCodeUpdate;
        this.permissionCodeDelete = permissionCodeDelete;
        this.name = name;
        this.dataTabController = dataTabController;
    }

    public int getPermissionCodeDelete() {
        return permissionCodeDelete;
    }

    public void setPermissionCodeDelete(int permissionCodeDelete) {
        this.permissionCodeDelete = permissionCodeDelete;
    }

    public int getPermissionCodeView() {
        return permissionCodeView;
    }

    public void setPermissionCodeView(int permissionCodeView) {
        this.permissionCodeView = permissionCodeView;
    }

    public int getPermissionCodeAdd() {
        return permissionCodeAdd;
    }

    public void setPermissionCodeAdd(int permissionCodeAdd) {
        this.permissionCodeAdd = permissionCodeAdd;
    }

    public int getPermissionCodeUpdate() {
        return permissionCodeUpdate;
    }

    public void setPermissionCodeUpdate(int permissionCodeUpdate) {
        this.permissionCodeUpdate = permissionCodeUpdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataTabController<?> getDataTabController() {
        return dataTabController;
    }

    public void setDataTabController(DataTabController<?> dataTabController) {
        this.dataTabController = dataTabController;
    }
}
