package com.alex.docdb.controller.tab;

import com.alex.docdb.App;
import com.alex.docdb.controller.dialog.AddGroupPermissionDialog;
import com.alex.docdb.controller.dialog.AddPermissionDialog;
import com.alex.docdb.db.impl.GroupPermissionDAO;
import com.alex.docdb.model.user.GroupPermission;
import com.alex.docdb.model.user.Permission;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;
import java.util.Optional;

public class GroupPermissionDataController extends DataTabController<GroupPermission> {

    private GroupPermissionDAO groupPermissionDAO;

    public GroupPermissionDataController() {
        groupPermissionDAO = new GroupPermissionDAO();
    }

    @Override
    protected void setUpUI(TableView<GroupPermission> tableView, List<GroupPermission> data) {

        TableColumn<GroupPermission, Integer> idColumn = new TableColumn<>("id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableView.getColumns().add(idColumn);

        TableColumn<GroupPermission, Integer> codeColumn = new TableColumn<>("code");
        codeColumn.setCellValueFactory(new PropertyValueFactory<>("code"));
        tableView.getColumns().add(codeColumn);
    }

    @Override
    protected List<GroupPermission> getData() {
        return groupPermissionDAO.findAll();
    }

    @Override
    public void onDelete() {
        GroupPermission p = getTableView().getSelectionModel().getSelectedItem();
        groupPermissionDAO.deleteById(p.getId());
        getTableView().getItems().remove(p);
    }

    @Override
    public void onInsert() {
        AddGroupPermissionDialog addGroupPermissionDialog = new AddGroupPermissionDialog(App.getStage());
        Optional<GroupPermission> permissionOptional = addGroupPermissionDialog.showAndWait();
        if (permissionOptional.isPresent()) {
            if (groupPermissionDAO.create(permissionOptional.get())) {
                getTableView().getItems().add(permissionOptional.get());
            }
        }
    }

    @Override
    public void onUpdate() {
        AddGroupPermissionDialog addGroupPermissionDialog = new AddGroupPermissionDialog(App.getStage(),
                getTableView().getSelectionModel().getSelectedItem());
        Optional<GroupPermission> permissionOptional = addGroupPermissionDialog.showAndWait();
        if (permissionOptional.isPresent()) {
            groupPermissionDAO.update(permissionOptional.get());
            getTableView().getItems().clear();
            getTableView().getItems().addAll(getData());
        }
    }
}
