package com.alex.docdb.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    public static Connection open() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/docdb", "root", "12345");
        } catch (ClassNotFoundException | SQLException ignored){
            return null;
        }
    }

}
