package com.alex.docdb.db;

import java.util.List;

public abstract class DAO<K, T> {

    public abstract List<T> findAll();

    public abstract T findEntityById(K id);

    public abstract boolean deleteById(K id);

    public abstract boolean delete(T entity);

    public abstract boolean create(T entity);

    public abstract T update(T entity);
}
