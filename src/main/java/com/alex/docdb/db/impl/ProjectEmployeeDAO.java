package com.alex.docdb.db.impl;

import com.alex.docdb.db.ConnectionUtil;
import com.alex.docdb.db.DAO;
import javafx.util.Pair;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectEmployeeDAO extends DAO<Integer, Pair<Integer, Integer>> {

    private static final String SQL_SELECT_ALL_RECORDS = "select * from project_employee";
    private static final String SQL_DELETE_RECORD_BY_ID = "delete from project_employee p where project_employee.project = ?";
    private static final String SQL_CREATE_RECORD = "insert into project_employee values (?, ?)";
    private static final String SQL_UPDATE_RECORD = "update project_employee set project_employee.project = ?, " +
            "project_employee.employee = ? where project_employee.project = ?";


    @Override
    public List<Pair<Integer, Integer>> findAll() {
        List<Pair<Integer, Integer>> result = new ArrayList<>();
        try (Connection connection = ConnectionUtil.open();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_RECORDS);
            while (resultSet.next()) {
                int first = resultSet.getInt(1);
                int second = resultSet.getInt(2);
                result.add(new Pair<>(first, second));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return result;
    }

    @Override
    public Pair<Integer, Integer> findEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean deleteById(Integer id) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_RECORD_BY_ID)) {
        preparedStatement.setInt(1, id);
        preparedStatement.execute();
        } catch (SQLException sqlException) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Pair<Integer, Integer> entity) {
        return false;
    }

    @Override
    public boolean create(Pair<Integer, Integer> entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_RECORD)) {
            preparedStatement.setInt(1, entity.getKey());
            preparedStatement.setInt(2, entity.getValue());
            preparedStatement.execute();
        } catch (SQLException sqlException) {
            return false;
        }
        return true;
    }

    @Override
    public Pair<Integer, Integer> update(Pair<Integer, Integer> entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_RECORD)) {
            preparedStatement.setInt(1, entity.getKey());
            preparedStatement.setInt(2, entity.getValue());
            preparedStatement.setInt(3, entity.getKey());
            preparedStatement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return null;
        }
        return entity;
    }
}
