package com.alex.docdb.db.impl;

import com.alex.docdb.db.ConnectionUtil;
import com.alex.docdb.db.DAO;
import com.alex.docdb.model.doc.Employee;
import com.alex.docdb.model.user.Group;
import com.alex.docdb.model.user.GroupPermission;
import com.alex.docdb.model.user.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO extends DAO<Integer, Employee> {

   private static final String SQL_SELECT_ALL_EMPLOYEES = "select * from Employee";
   private static final String SQL_DELETE_EMPLOYEE_BY_ID = "delete from employee where id = ?";
   private static final String SQL_CREATE_EMPLOYEE = "insert into employee values (?, ?, ?, ?, ?, ?)";
   private static final String SQL_UPDATE_EMPLOYEE = "update employee set firstname = ?, lastname = ?, age = ?, position = ?, " +
           "experience = ? where id = ?";

    @Override
    public List<Employee> findAll() {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = ConnectionUtil.open();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL_EMPLOYEES);
            while (rs.next()) {
                int id = rs.getInt(1);
                String firstname = rs.getString(2);
                String lastname = rs.getString(3);
                String age = rs.getString(4);
                String position = rs.getString(5);
                int experience = rs.getInt(6);
                employees.add(new Employee(id, firstname, lastname, Integer.parseInt(age), position, experience));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return employees;
    }

    @Override
    public Employee findEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean deleteById(Integer id) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_EMPLOYEE_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Employee entity) {
        return false;
    }

    @Override
    public boolean create(Employee entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_EMPLOYEE)) {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setString(2, entity.getFirstname());
            preparedStatement.setString(3, entity.getLastname());
            preparedStatement.setString(4, Integer.toString(entity.getAge()));
            preparedStatement.setString(5, entity.getPosition());
            preparedStatement.setInt(6, entity.getExperience());
            preparedStatement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public Employee update(Employee entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_EMPLOYEE)) {
            preparedStatement.setInt(6, entity.getId());
            preparedStatement.setString(1, entity.getFirstname());
            preparedStatement.setString(2, entity.getLastname());
            preparedStatement.setString(3, Integer.toString(entity.getAge()));
            preparedStatement.setString(4, entity.getPosition());
            preparedStatement.setInt(5, entity.getExperience());
            preparedStatement.execute();
        } catch (SQLException e) {
            return null;
        }
        return entity;
    }
}
