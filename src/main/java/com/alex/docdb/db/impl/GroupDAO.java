package com.alex.docdb.db.impl;

import com.alex.docdb.db.ConnectionUtil;
import com.alex.docdb.db.DAO;
import com.alex.docdb.db.exceptions.EntityNotFoundException;
import com.alex.docdb.model.user.Group;
import com.alex.docdb.model.user.GroupPermission;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GroupDAO extends DAO<Integer, Group> {

    private static final String SQL_SELECT_ALL_GROUPS = "select g.id, g.name, g.permissions, " +
            "p.code from docdb.Group g inner join Group_Permissions p on g.permissions = p.id;";
    private static final String SQL_SELECT_GROUP_BY_ID = "select g.id, g.name, g.permissions, " +
            "p.code from docdb.Group g inner join Group_Permissions p on g.permissions = p.id where g.id = ?;";
    private static final String SQL_DELETE_GROUP_BY_ID = "delete from docdb.Group where id = ?";
    private static final String SQL_CREATE_GROUP = "insert into docdb.group values (?, ?, ?)";
    private static final String SQL_UPDATE_GROUP = "update docdb.group set name = ?, permissions = ? where id = ?";

    @Override
    public List<Group> findAll() {
        List<Group> groups = new ArrayList<>();
        try (Connection connection = ConnectionUtil.open();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL_GROUPS);
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                int permissions = rs.getInt(3);
                int code = rs.getInt(4);
                GroupPermission groupPermission = new GroupPermission(permissions, code);
                groups.add(new Group(id, name, groupPermission));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return groups;
    }

    @Override
    public Group findEntityById(Integer id) {
        Group group = null;
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_GROUP_BY_ID)) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String name = rs.getString(2);
                int permissions = rs.getInt(3);
                int code = rs.getInt(4);
                GroupPermission groupPermission = new GroupPermission(permissions, code);
                group = new Group(id, name, groupPermission);
            } else {
                throw new EntityNotFoundException("Can't find Group with id " + id);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return group;
    }

    @Override
    public boolean deleteById(Integer id) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_GROUP_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Group entity) {
        return false;
    }

    @Override
    public boolean create(Group entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_CREATE_GROUP)) {
            statement.setInt(1, entity.getId());
            statement.setString(2, entity.getName());
            statement.setInt(3, entity.getGroupPermission().getId());
            statement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public Group update(Group entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GROUP)) {
            statement.setString(1, entity.getName());
            statement.setInt(2, entity.getGroupPermission().getId());
            statement.setInt(3, entity.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return entity;
    }
}
