package com.alex.docdb.db.impl;

import com.alex.docdb.db.ConnectionUtil;
import com.alex.docdb.db.DAO;
import com.alex.docdb.db.exceptions.EntityNotFoundException;
import com.alex.docdb.model.user.Group;
import com.alex.docdb.model.user.GroupPermission;
import com.alex.docdb.model.user.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO extends DAO<String, User> {

    private static final String SQL_SELECT_ALL_USERS = "select u.id, u.username, u.password, u.group, " +
            "g.id, g.name, g.permissions, p.code from User u inner join docdb.Group g on u.group = g.id " +
            "inner join Group_Permissions p on g.permissions = p.id;";
    private static final String SQL_SELECT_USER_BY_ID = "select u.id, u.username, u.password, u.group, " +
            "g.id, g.name, g.permissions, p.code from User u inner join docdb.Group g on u.group = g.id " +
            "inner join Group_Permissions p on g.permissions = p.id where u.username = ?;";
    private static final String SQL_DELETE_USER_BY_ID = "delete from User where id = ?";
    private static final String SQL_CREATE_USER = "insert into User values (?, ?, ?, ?)";
    private static final String SQL_UPDATE_USER = "update user set id = ?, username = ?, password = ?, user.group = ? where id = ?";

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (Connection connection = ConnectionUtil.open();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL_USERS);
            while (rs.next()) {
                int id = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                int groupId = rs.getInt(4);
                String name = rs.getString(6);
                int permissions = rs.getInt(7);
                int code = rs.getInt(8);
                GroupPermission groupPermission = new GroupPermission(permissions, code);
                Group group = new Group(groupId, name, groupPermission);
                users.add(new User(id, username, password, group));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return users;
    }

    @Override
    public User findEntityById(String id) {
        User user = null;
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USER_BY_ID)) {
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                int uId = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                int groupId = rs.getInt(4);
                String name = rs.getString(6);
                int permissions = rs.getInt(7);
                int code = rs.getInt(8);
                GroupPermission groupPermission = new GroupPermission(permissions, code);
                Group group = new Group(groupId, name, groupPermission);
                user = new User(uId, username, password, group);
            } else {
                throw new EntityNotFoundException(String.format("Can't find User with id '%s'", id));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return user;
    }

    @Override
    public boolean deleteById(String id) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_USER_BY_ID)) {
            statement.setString(1, id);
            statement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(User entity) {
        return false;
    }

    @Override
    public boolean create(User entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_CREATE_USER)) {
            statement.setInt(1, entity.getId());
            statement.setString(2, entity.getUsername());
            statement.setString(3, entity.getPassword());
            statement.setInt(4, entity.getGroup().getId());
            statement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public User update(User entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER)) {
            statement.setInt(1, entity.getId());
            statement.setString(2, entity.getUsername());
            statement.setString(3, entity.getPassword());
            statement.setInt(4, entity.getGroup().getId());
            statement.setInt(5, entity.getId());
            statement.execute();
        } catch (SQLException e) {
            return null;
        }
        return entity;
    }
}
