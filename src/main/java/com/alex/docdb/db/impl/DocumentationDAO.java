package com.alex.docdb.db.impl;

import com.alex.docdb.db.ConnectionUtil;
import com.alex.docdb.db.DAO;
import com.alex.docdb.model.doc.Documentation;
import com.alex.docdb.model.doc.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DocumentationDAO extends DAO<Integer, Documentation> {

    private static final String SQL_SELECT_ALL_DOCUMENTATION = "select * from Documentation";
    private static final String SQL_CREATE_DOCUMENTATION = "insert into documentation values (?, ?)";
    private static final String SQL_DELETE_DOCUMENTATION_BY_ID = "delete from documentation where id = ?";
    private static final String SQL_UPDATE_DOCUMENTATION = "update documentation set file_path = ? where id = ?";

    @Override
    public List<Documentation> findAll() {
        List<Documentation> documentations = new ArrayList<>();
        try (Connection connection = ConnectionUtil.open();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL_DOCUMENTATION);
            while (rs.next()) {
                int id = rs.getInt(1);
                String folderPath = rs.getString(2);
                documentations.add(new Documentation(id, folderPath));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return documentations;
    }

    @Override
    public Documentation findEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean deleteById(Integer id) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_DOCUMENTATION_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Documentation entity) {
        return false;
    }

    @Override
    public boolean create(Documentation entity) {
        try (Connection connection = ConnectionUtil.open();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_DOCUMENTATION)) {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setString(2, entity.getFolderPath());
            preparedStatement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public Documentation update(Documentation entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_DOCUMENTATION)) {
            preparedStatement.setString(1, entity.getFolderPath());
            preparedStatement.setInt(2, entity.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            return null;
        }
        return entity;
    }
}
