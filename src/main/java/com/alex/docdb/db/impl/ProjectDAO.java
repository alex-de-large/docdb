package com.alex.docdb.db.impl;

import com.alex.docdb.db.ConnectionUtil;
import com.alex.docdb.db.DAO;
import com.alex.docdb.model.doc.Documentation;
import com.alex.docdb.model.doc.Employee;
import com.alex.docdb.model.doc.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectDAO extends DAO<Integer, Project> {

    private static final String SQL_SELECT_ALL_PROJECTS = "select p.id, p.name, " +
            "d.id, d.file_path from Project p inner join Documentation d on p.Documentation_id = d.id";
    private static final String SQL_SELECT_ALL_PROJECT_EMPLOYEES =
            "select * from Employee where id in (select employee from Project_Employee where project = ?);";
    private static final String SQL_DELETE_PROJECT_BY_ID = "delete from project where id = ?";
    private static final String SQL_CREATE_PROJECT = "insert into project values (?, ?, ?)";
    private static final String SQL_UPDATE_PROJECT = "update project set name = ?, Documentation_id = ? where id = ?";

    @Override
    public List<Project> findAll() {
        List<Project> projects = new ArrayList<>();
        try (Connection connection = ConnectionUtil.open();
             Statement statement = connection.createStatement();
             PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL_PROJECT_EMPLOYEES);) {
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL_PROJECTS);
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                int docId = rs.getInt(3);
                String folderPath = rs.getString(4);
                st.setString(1, Integer.toString(id));
                List<Employee> employees = new ArrayList<>();
                ResultSet r = st.executeQuery();
                while (r.next()) {
                    int employeeId = r.getInt(1);
                    String firstname = r.getString(2);
                    String lastname = r.getString(3);
                    String age = r.getString(4);
                    String position = r.getString(5);
                    int experience = r.getInt(6);
                    employees.add(new Employee(employeeId, firstname, lastname, Integer.parseInt(age), position, experience));
                }
                projects.add(new Project(id, name, new Documentation(docId, folderPath), employees));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projects;
    }

    @Override
    public Project findEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean deleteById(Integer id) {
        try (Connection connection = ConnectionUtil.open();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_PROJECT_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Project entity) {
        return false;
    }

    @Override
    public boolean create(Project entity) {
        try (Connection connection = ConnectionUtil.open();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_PROJECT)) {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setInt(3, entity.getDocumentation().getId());
            preparedStatement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Project update(Project entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_PROJECT)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getDocumentation().getId());
            preparedStatement.setInt(3, entity.getId());
            preparedStatement.execute();
        } catch (SQLException sqlException) {
            return null;
        }
        return entity;
    }
}
