package com.alex.docdb.db.impl;

import com.alex.docdb.db.ConnectionUtil;
import com.alex.docdb.db.DAO;
import com.alex.docdb.model.user.Permission;
import com.alex.docdb.model.user.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PermissionDAO extends DAO<Integer, Permission> {

    private static final String SQL_SELECT_ALL_PERMISSIONS = "select * from Permission";
    private static final String SQL_SELECT_PERMISSION_BY_ID = "select * from Permission where id = ?";
    private static final String SQL_DELETE_PERMISSION_BY_ID = "delete from Permission where id = ?";
    private static final String SQL_CREATE_PERMISSION = "insert into Permission values (?, ?)";
    private static final String SQL_UPDATE_PERMISSION = "update permission set id = ?, name = ? where id = ?";

    @Override
    public List<Permission> findAll() {
        List<Permission> permissions = new ArrayList<>();
        try (Connection connection = ConnectionUtil.open();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL_PERMISSIONS);
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                permissions.add(new Permission(id, name));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return permissions;
    }

    @Override
    public Permission findEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean deleteById(Integer id) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_PERMISSION_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Permission entity) {
        return false;
    }

    @Override
    public boolean create(Permission entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_CREATE_PERMISSION)) {
            statement.setInt(1, entity.getId());
            statement.setString(2, entity.getName());
            statement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public Permission update(Permission entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PERMISSION)) {
            statement.setInt(1, entity.getId());
            statement.setString(2, entity.getName());
            statement.setInt(3, entity.getId());
            statement.execute();
        } catch (SQLException e) {
            return null;
        }
        return entity;
    }
}
