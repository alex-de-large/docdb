package com.alex.docdb.db.impl;

import com.alex.docdb.db.ConnectionUtil;
import com.alex.docdb.db.DAO;
import com.alex.docdb.db.exceptions.EntityNotFoundException;
import com.alex.docdb.model.user.GroupPermission;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GroupPermissionDAO extends DAO<Integer, GroupPermission> {

    private static final String SQL_SELECT_ALL_GROUP_PERMISSIONS = "select * from Group_Permissions";
    private static final String SQL_SELECT_GROUP_PERMISSION_BY_ID = "select * from Group_Permissions where id = ?";
    private static final String SQL_DELETE_GROUP_PERMISSION_BY_ID = "delete from Group_Permissions where id = ?";
    private static final String SQL_CREATE_GROUP_PERMISSION = "insert into Group_Permissions values (?, ?)";
    private static final String SQL_UPDATE_GROUP_PERMISSION = "update Group_Permissions set code = ? where id = ?";


    @Override
    public List<GroupPermission> findAll() {
        List<GroupPermission> groupPermissions = new ArrayList<>();
        try (Connection connection = ConnectionUtil.open();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL_GROUP_PERMISSIONS);
            while (rs.next()) {
                int id = rs.getInt(1);
                int code = rs.getInt(2);
                groupPermissions.add(new GroupPermission(id, code));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return groupPermissions;
    }

    @Override
    public GroupPermission findEntityById(Integer id) {
        GroupPermission groupPermission = null;
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_GROUP_PERMISSION_BY_ID)) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                int code = rs.getInt(2);
                groupPermission = new GroupPermission(id, code);
            } else {
                throw new EntityNotFoundException("Can't find GroupPermission with id " + id);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return groupPermission;
    }

    @Override
    public boolean deleteById(Integer id) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_GROUP_PERMISSION_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(GroupPermission entity) {
        return false;
    }

    @Override
    public boolean create(GroupPermission entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_CREATE_GROUP_PERMISSION)) {
            statement.setInt(1, entity.getId());
            statement.setInt(2, entity.getCode());
            statement.execute();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public GroupPermission update(GroupPermission entity) {
        try (Connection connection = ConnectionUtil.open();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GROUP_PERMISSION)) {
            statement.setInt(2, entity.getId());
            statement.setInt(1, entity.getCode());
            statement.execute();
        } catch (SQLException e) {
            return null;
        }
        return entity;
    }
}
