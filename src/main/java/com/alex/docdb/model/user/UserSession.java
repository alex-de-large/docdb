package com.alex.docdb.model.user;

public final class UserSession {

    private static UserSession userSession;

    private User user;

    private UserSession(User user) {
        this.user = user;
    }

    public static UserSession get(User user) {
        if (userSession == null) {
            userSession = new UserSession(user);
        }
        return userSession;
    }

    public User getUser() {
        return user;
    }

    public void clear() {
        user = null;
    }
}
