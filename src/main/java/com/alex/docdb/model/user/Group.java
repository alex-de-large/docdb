package com.alex.docdb.model.user;

public class Group {

    private int id;
    private String name;
    private GroupPermission groupPermission;

    public Group(int id, String name, GroupPermission groupPermission) {
        this.id = id;
        this.name = name;
        this.groupPermission = groupPermission;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GroupPermission getGroupPermission() {
        return groupPermission;
    }

    public void setGroupPermission(GroupPermission groupPermission) {
        this.groupPermission = groupPermission;
    }

    public boolean hasPermission(int permissionCode) {
        return (groupPermission.getCode() & permissionCode) == permissionCode;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", groupPermission=" + groupPermission +
                '}';
    }
}
