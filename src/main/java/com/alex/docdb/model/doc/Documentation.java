package com.alex.docdb.model.doc;

public class Documentation {

    private int id;
    private String folderPath;

    public Documentation() {

    }

    public Documentation(int id, String folderPath) {
        this.id = id;
        this.folderPath = folderPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }
}
