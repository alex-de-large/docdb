package com.alex.docdb.model.doc;

import java.util.List;

public class Project {

    private int id;
    private String name;
    private Documentation documentation;
    private List<Employee> employees;

    public Project() {

    }

    public Project(int id, String name, Documentation documentation, List<Employee> employees) {
        this.id = id;
        this.name = name;
        this.documentation = documentation;
        this.employees = employees;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Documentation getDocumentation() {
        return documentation;
    }

    public void setDocumentation(Documentation documentation) {
        this.documentation = documentation;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
