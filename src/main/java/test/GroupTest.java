package test;

import com.alex.docdb.model.user.Group;
import com.alex.docdb.model.user.GroupPermission;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class GroupTest {

    private Group group;

    @Before
    public void setup() {
        group = new Group(1, "test", new GroupPermission(1, 127));
    }

    @Test
    public void testPermissionsCheck() {
        assertTrue(group.hasPermission(1));
        assertTrue(group.hasPermission(2));
        assertTrue(group.hasPermission(4));
        assertTrue(group.hasPermission(8));
        assertTrue(group.hasPermission(16));
        assertTrue(group.hasPermission(32));
        assertTrue(group.hasPermission(64));
        assertFalse(group.hasPermission(128));
    }
}
