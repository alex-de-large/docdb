-- MySQL Script generated by MySQL Workbench
-- Mon Sep 27 02:30:57 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

drop database docdb;
CREATE database docdb;
use docdb;

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


CREATE TABLE IF NOT EXISTS `Group_Permissions` (
  `id` INT NOT NULL,
  `code` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Group` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `permissions` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Group_Group_Right1_idx` (`permissions` ASC) VISIBLE,
  CONSTRAINT `fk_Group_Group_Right1`
    FOREIGN KEY (`permissions`)
    REFERENCES `Group_Permissions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `User` (
  `id` INT NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(32) NOT NULL,
  `group` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_Group_idx` (`group` ASC) VISIBLE,
  CONSTRAINT `fk_user_Group`
    FOREIGN KEY (`group`)
    REFERENCES `Group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `Permission`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Permission` (
  `id` INT NOT NULL,
  `name` VARCHAR(15) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Documentation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Documentation` (
  `id` INT NOT NULL,
  `file_path` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Project` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `Documentation_id` INT NOT NULL,
  PRIMARY KEY (`id`, `Documentation_id`),
  INDEX `fk_Project_Documentation1_idx` (`Documentation_id` ASC) VISIBLE,
  CONSTRAINT `fk_Project_Documentation1`
    FOREIGN KEY (`Documentation_id`)
    REFERENCES `Documentation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Employee` (
  `id` INT NOT NULL,
  `firstname` VARCHAR(45) NULL,
  `lastname` VARCHAR(45) NULL,
  `age` VARCHAR(45) NULL,
  `position` VARCHAR(45) NULL,
  `experience` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Project_Employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Project_Employee` (
  `project` INT NOT NULL,
  `employee` INT NOT NULL,
  PRIMARY KEY (`project`, `employee`),
  INDEX `fk_Project_has_Employee_Employee1_idx` (`employee` ASC) VISIBLE,
  INDEX `fk_Project_has_Employee_Project1_idx` (`project` ASC) VISIBLE,
  CONSTRAINT `fk_Project_has_Employee_Project1`
    FOREIGN KEY (`project`)
    REFERENCES `Project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Project_has_Employee_Employee1`
    FOREIGN KEY (`employee`)
    REFERENCES `Employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


insert into docdb.Permission values (1, "Project-View");
insert into docdb.Permission values (2, "Project-Edit");
insert into docdb.Permission values (4, "Project-Add");
insert into docdb.Permission values (8, "Project-Delete");
insert into docdb.Permission values (16, "User-View");
insert into docdb.Permission values (32, "User-Edit");
insert into docdb.Permission values (64, "User-Add");
insert into docdb.Permission values (128, "User-Delete");

insert into Group_Permissions values (1, 255);
insert into docdb.Group values (1, "admin", 1);
insert into User values (1, "alex", "alex", 1);
